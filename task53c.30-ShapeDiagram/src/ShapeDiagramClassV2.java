import dev.*;

public class ShapeDiagramClassV2 {
    public static void main(String[] args) throws Exception {
    Rectangle rectangle = new Rectangle(3,4);
    Circle circle = new Circle(2);
    Square square = new Square(5);
    System.out.println(rectangle);
    System.out.println("DT hinh chu nhat: " + rectangle.getArea());
    System.out.println("CV hinh chu nhat: " + rectangle.getPerimeter());
    System.out.println(circle);
    System.out.println("DT hinh tron: " + circle.getArea());
    System.out.println("CV hinh tron: " + circle.getPerimeter());
    System.out.println(square);
    System.out.println("DT hinh vuong: " + square.getArea());
    System.out.println("CV hinh vuong: " + square.getPerimeter());
    }
}
